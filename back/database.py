from rethinkdb import RethinkDB
from .config import load_parameter, requires_config

NEEDED_TABLES = ["users", "tokens"]


@requires_config("DATABASE_PORT", "DATABASE_HOST")
def db() -> RethinkDB:
    conn = RethinkDB()
    conn.connect(
        load_parameter("DATABASE_HOST"), load_parameter("DATABASE_PORT")
    ).repl()
    return conn


def init_db(conn: RethinkDB):
    tables_in_db = conn.table_list().run()
    for table in NEEDED_TABLES:
        if table not in tables_in_db:
            conn.table_create(table).run()
