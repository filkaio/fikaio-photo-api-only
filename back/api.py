from flask import Blueprint
from .photos import Photos
from .restx import FixedApi


bp = Blueprint("api", __name__)
api = FixedApi(bp, version="1.0", title="Filka.io API")

api.add_namespace(Photos, path="/photos")

