stub = {"event_id": 70, "name": "seeding"}


class CalendarException(Exception):
    """Calendar Exception"""

    def __init__(self, message):
        super().__init__(message)


def add_event(event):
    _ = event
    return 70


def get_event(event_id):
    if event_id == 70:
        return stub
    raise CalendarException("Event ID not found")


def get_all_events():
    return [stub]


def update_event(event_id, event):
    _ = event
    if event_id != 70:
        raise CalendarException("Event ID not found")
    return stub


def delete_event(event_id):
    if event_id != 70:
        raise CalendarException("Event ID not found")
