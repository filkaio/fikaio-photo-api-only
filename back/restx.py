from flask_restx import Api
from functools import partial
import warnings

"""
Those are the fixes needed to silence and patch restx library.
2 functions below can be deleted once this gets merged:
https://github.com/python-restx/flask-restx/pull/500

Next 2 functions overloaded properties from lib and are needed for use with nginx
proxy.
"""


def init_app(self, app, **kwargs):
    """
    Allow to lazy register the API on a Flask application::

    >>> app = Flask(__name__)
    >>> api = Api()
    >>> api.init_app(app)

    :param flask.Flask app: the Flask application object
    :param str title: The API title (used in Swagger documentation)
    :param str description: The API description (used in Swagger documentation)
    :param str terms_url: The API terms page URL (used in Swagger documentation)
    :param str contact: A contact email for the API (used in Swagger documentation)
    :param str license: The license associated to the API (used in Swagger documentation)
    :param str license_url: The license page URL (used in Swagger documentation)
    :param url_scheme: If set to a string (e.g. http, https), then the specs_url and base_url will explicitly use
        this scheme regardless of how the application is deployed. This is necessary for some deployments behind a
        reverse proxy.
    """
    self.app = app
    self.title = kwargs.get("title", self.title)
    self.description = kwargs.get("description", self.description)
    self.terms_url = kwargs.get("terms_url", self.terms_url)
    self.contact = kwargs.get("contact", self.contact)
    self.contact_url = kwargs.get("contact_url", self.contact_url)
    self.contact_email = kwargs.get("contact_email", self.contact_email)
    self.license = kwargs.get("license", self.license)
    self.license_url = kwargs.get("license_url", self.license_url)
    self.url_scheme = kwargs.get("url_scheme", self.url_scheme)
    self._add_specs = kwargs.get("add_specs", True)
    self._register_specs(app)
    self._register_doc(app)

    # If app is a blueprint, defer the initialization
    try:
        app.record(self._deferred_blueprint_init)
    # Flask.Blueprint has a 'record' attribute, Flask.Api does not
    except AttributeError:
        self._init_app(app)
    else:
        self.blueprint = app


def _init_app(self, app):
    """
    Perform initialization actions with the given :class:`flask.Flask` object.

    :param flask.Flask app: The flask application object
    """
    app.handle_exception = partial(self.error_router, app.handle_exception)
    app.handle_user_exception = partial(self.error_router, app.handle_user_exception)

    if len(self.resources) > 0:
        for resource, namespace, urls, kwargs in self.resources:
            self._register_view(app, resource, namespace, *urls, **kwargs)

    for ns in self.namespaces:
        self._configure_namespace_logger(app, ns)

    self._register_apidoc(app)
    self._validate = (
        self._validate
        if self._validate is not None
        else app.config.get("RESTX_VALIDATE", False)
    )
    app.config.setdefault("RESTX_MASK_HEADER", "X-Fields")
    app.config.setdefault("RESTX_MASK_SWAGGER", True)
    app.config.setdefault("RESTX_INCLUDE_ALL_MODELS", False)

    # check for deprecated config variable names
    if "ERROR_404_HELP" in app.config:
        app.config["RESTX_ERROR_404_HELP"] = app.config["ERROR_404_HELP"]
        warnings.warn(
            "'ERROR_404_HELP' config setting is deprecated and will be "
            "removed in the future. Use 'RESTX_ERROR_404_HELP' instead.",
            DeprecationWarning,
        )


Api.init_app = init_app
Api._init_app = _init_app


class FixedApi(Api):
    """
    This is a fix for restx library. It does not account for reverse proxy and
    tries to download from "/swagger.json" endpoint which is a frontend path.
    """

    @property
    def specs_url(self):
        return "/api/swagger.json"

    @property
    def base_path(self):
        return "/api"
