from flask_restx import Namespace, Resource
from . import quote

ns = Namespace("Miscellaneous", description="Endpoints for various functionalities")


@ns.route("/healthcheck")
class Healthcheck(Resource):
    def get(self):
        return {
            "status": 0,
            "desc": "API is alive",
        }


@ns.route("/quoteOfTheDay")
class QouteOfTheDay(Resource):
    def get(self):
        return quote.get_quote_today()
