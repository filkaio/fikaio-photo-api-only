import os
from configparser import ConfigParser
from typing import Optional


class ConfigException(Exception):
    pass


class PrintOnce:
    @staticmethod
    def print(to_print):
        try:
            if to_print not in PrintOnce.already_printed:
                print(to_print)
        except AttributeError:
            PrintOnce.already_printed = [to_print]
            print(to_print)


def url():
    return load_parameter("SUPLA_BASE_URL")


def require_param(param):
    """
    Check if given parameter exists in configuration.
    Raises ConfigException if not.
    """
    if not load_parameter(param):
        raise ConfigException(f"{param} not found")


def requires_config(*params):
    """
    A decorator that will fail if config value does not exist.
    Can also be used as function.
    """
    for param in params:
        require_param(param)

    # Return original function
    # (Required to be used as decorator)
    def decorator(callback):
        return callback

    return decorator


def load_parameter(
    param_name: str, default_value: Optional[str] = None
) -> Optional[str]:
    if param := load_from_env(param_name):
        return param
    if param := load_from_file(param_name):
        return param
    if default_value is not None:
        return default_value
    print(f"Could not find parameter '{param_name}'")


def load_from_env(param_name: str) -> Optional[str]:
    return os.environ.get(param_name.upper())


def load_from_file(param_name: str) -> Optional[str]:
    dir = os.path.abspath(os.getcwd())
    path = os.path.join(dir, "backend.conf")
    PrintOnce.print(f"Config will be loaded from {path}")

    config = ConfigParser()
    config.read(path)

    try:
        if param := config["CONFIG"][param_name.upper()]:
            return param
    except KeyError:
        pass


class Config:
    SUPLA_CLIENT_ID = load_parameter("SUPLA_CLIENT_ID")
    SUPLA_CLIENT_SECRET = load_parameter("SUPLA_CLIENT_SECRET")
