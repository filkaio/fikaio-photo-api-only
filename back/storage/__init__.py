from .abstract import AbstractStorageClient
from .local import LocalStorageClient
from .storjio import StrojioStorageClient


def get_available_clients():
    clients = [
        AbstractStorageClient,
        LocalStorageClient,
        StrojioStorageClient
    ]
    return {
        c.__name__: c
        for c in clients
    }
