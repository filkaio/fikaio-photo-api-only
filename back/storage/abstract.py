from abc import ABC, abstractmethod


class StorageException(Exception):
    pass

class AbstractStorageClient(ABC):

    @abstractmethod
    def file_exists(self, filename):
        pass

    @abstractmethod
    def stream_to_file(self, filename, stream):
        pass

    @abstractmethod
    def get_file(self, filename):
        pass


