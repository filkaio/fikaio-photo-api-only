from back import supla_client
from typing import Any
from . import db
import pandas as pd


## Api functions


def insert_or_update_user(token):
    supla_user = supla_client.get_user(token)
    user = db().table("users").get(supla_user["id"])
    if user.run():
        user.update({"token": token}).run()
    else:
        add_user(supla_user, token)
    return supla_user["id"]


def get_sensors(user):
    sensors = supla_client.get_sensors(user)
    return [{"id": sensor["id"], "name": sensor["caption"]} for sensor in sensors]


def get_readouts(sensor_id, limit, user):
    readouts = supla_client.get_readouts(user, sensor_id, limit)
    if readouts is None:
        return []
    return parse_readouts_responese(readouts)


def get_sensor_stats(sensor_id, hours, user):
    readouts = supla_client.get_readouts(user, sensor_id, 5000)
    if readouts is None:
        return []
    return parse_stats(readouts, hours * 60 * 60)


## Helper functions


def add_user(supla_user, token):
    new_user = {
        "id": supla_user["id"],
        "email": supla_user["email"],
        "locale": supla_user["locale"],
        "timezone": supla_user["timezone"],
        "token": token,
    }
    db().table("users").insert(new_user).run()


def parse_stats(readouts, period_seconds):
    """
    making DataFrame format
    """
    df = pd.DataFrame.from_records(readouts)
    timestamp: Any = df["date_timestamp"]  # type given to supress linting errors
    df["time"] = pd.to_datetime(timestamp, unit="s")
    df["date_timestamp"] = pd.to_numeric(df["date_timestamp"])
    df["temperature"] = pd.to_numeric(df["temperature"])
    df["humidity"] = pd.to_numeric(df["humidity"])
    df.set_index("date_timestamp")

    stats = df.loc[
        df.date_timestamp > df.iloc[0]["date_timestamp"] - period_seconds, :
    ]  # reaching stats from desire time period

    dataVals = [
        "min",
        "max",
        "mean",
        "std",
        "median",
    ]  # desired values from ['count', 'mean', 'std', 'min', '25%', '50%', '75%', 'max']

    agregatedStats = stats.agg({"temperature": dataVals, "humidity": dataVals})

    data = convert_agregated_stats(agregatedStats, df)
    return data


def parse_readouts_responese(readouts):
    """
    This response is for ApexCharts format
    """
    data = [
        {"name": "humidity", "data": []},
        {"name": "temperature", "data": []},
    ]
    for datapoint in readouts:
        time = int(datapoint["date_timestamp"]) * 1000
        data[0]["data"].append(
            {
                "x": time,
                "y": float(datapoint["humidity"]),
            }
        )
        data[1]["data"].append(
            {
                "x": time,
                "y": float(datapoint["temperature"]),
            }
        )
    return data


def convert_agregated_stats(agregatedStats, records):
    """converting stats to desired JSON"""
    data = {}
    for label in list(agregatedStats.columns):
        params = {}
        for row in agregatedStats.index:
            if row in ["max", "min"]:  # TODO: add idmax and idmin instead of it
                time = records.loc[
                    records[label] == agregatedStats[label][row]
                ]  # localize time for desired valuexs
                params[row] = {
                    "value": int(agregatedStats[label][row]),
                    "time": int(time["date_timestamp"].values[0]),
                }
            else:
                params[row] = int(agregatedStats[label][row])
                data[label] = params
    return data
