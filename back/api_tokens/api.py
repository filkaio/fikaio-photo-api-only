from http import HTTPStatus
from flask_restx import Namespace, Resource
from flask_restx.reqparse import RequestParser
from . import logic
from werkzeug.exceptions import BadRequest
from back.api_helpers import requires_login

ns = Namespace(name="Tokens", description="Manage user tokens")

new_token_parser = RequestParser()
new_token_parser.add_argument("name", type=str, location="json")


@ns.route("/")
class Tokens(Resource):
    @ns.doc("Get all tokens for user")
    @requires_login
    def get(self, user):
        return logic.get_tokens_for_user(user)

    @ns.doc("Create new token")
    @ns.expect(new_token_parser)
    @requires_login
    def post(self, user):
        try:
            name = new_token_parser.parse_args()["name"]
            return logic.create_token(user, name)
        except logic.TokenException as e:
            raise BadRequest(str(e))


@ns.route("/id/<id>")
class TokenById(Resource):
    @ns.doc("Get this token")
    @requires_login
    def get(self, user, id):
        err_msg = "Unable to get this token"
        try:
            token = logic.get_token_by_id(id)
            if token["user"] != user["id"]:
                raise BadRequest(err_msg)
            return token
        except logic.TokenException:
            raise BadRequest(err_msg)

    @ns.doc("Delete this token")
    @requires_login
    def delete(self, user, id):
        err_msg = "Unable to delete this token"
        try:
            token = logic.get_token_by_id(id)
            if token["user"] != user["id"]:
                raise BadRequest(err_msg)
            logic.delete_token_by_id(token["id"])
            return "", HTTPStatus.NO_CONTENT
        except logic.TokenException:
            raise BadRequest(err_msg)
