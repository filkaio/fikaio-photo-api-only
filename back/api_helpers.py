def err(msg, logout=False):
    return {"err": True, "logout": logout, "decription": msg}


def requires_login(callback):
    """
    Use this to authorize user before handling the request. Function must take
    `user` as the argument (or kwarg).
    """

    def wrapper(*args, **kwargs):
        return callback(*args, user=None, **kwargs)

    # This line is needed because Flask view functions are name-sensitive
    # https://stackoverflow.com/q/17256602
    wrapper.__name__ = callback.__name__
    return wrapper

