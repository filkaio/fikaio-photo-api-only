#!/bin/bash

sudo -n true
WAIT_FOR_PASSWORD="$?"
if [ "$WAIT_FOR_PASSWORD" != "0" ]; then
  echo "You have 7 seconds to input sudo password"
fi

sudo true

sudo -n true
if [ "$?" != "0" ]; then
  echo "That won't work... "
  echo "Sorry but this script relies on your sudo remembering password."
  echo "For now you can try looking into what the script does and do it manually"
  exit 1
fi

