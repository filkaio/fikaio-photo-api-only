#!/bin/bash

e="echo -e"

$e "\n\n"
$e "\t\tPlease activate virtual environment befor attempting to run the project"
$e "\t\tFor bash or any other unix-compatible shell type this command:"
$e "\t\t. venv/bin/activate"
$e "\n\n"

