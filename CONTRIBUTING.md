CONTRIBUTING

# New contributor guide

To get an overview of the project, ![read the README.md file(/README.md)

# Issues

## Creating an issue

If you spot a problem with the app, search if an issue already exists. Here you can review our ![issue board](https://gitlab.com/climco/climco/-/boards/4778548). If a related issue doesn't exist, please open a new issue using to the Board.

## Solving an issue

Scan through our existing issues to find one that interests you. You can narrow down the search using labels as filters. See Labels for more information. If you find an issue to work on, you are welcome to work on it. Please, assign the issue to yourself and add some commentary about what the Issue is about.

# Making changes

## Our practices

If you want to help, please use the same code style we use. We use automatic tests, so code significantly different stylistically might cause error within the pipeline.

## How to contribute

If you want to help our project and add any changes to the code, please:
- fork the repository
- make changes you want to add
- commit and push your changes
- create pull request - your pull request will be reviewed by a member of filka.io team 

